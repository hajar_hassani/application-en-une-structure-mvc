<?php
/*
  ./app/vues/posts/index.php
  Variables disponibles:
  - $posts: ARRAY(ARRAY(id, nom))
*/
?>

<?php foreach ($tags as $tag): ?>
  <ul class="list-unstyled mb-0">
    <li>
      <a href="#"><?php echo $tag['nom']  ?></a> | <a href="tags/<?php echo $tag['id']; ?>/<?php echo \Noyau\fonctions\slugify($tag['nom']); ?>/edit/form.html">Modifier</a
    </li>
  </ul>
<?php endforeach; ?>
