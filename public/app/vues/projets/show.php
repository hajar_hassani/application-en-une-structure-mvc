<?php
/*
  ./app/vues/projets/show.php
  Variables disponibles:
  - $projets: ARRAY(projetId, titre, texte, dateCreation, projetImage, creatif)
*/
?>

  <!-- Page Heading -->

  <!-- Title -->
  <h1 class="mt-4"><?php echo $projet['titre']; ?></h1>
  <p class="lead">
    par
    <a href="artiste_details.html"<?php echo $projet['pseudo']; ?></a>
    le <?php echo \Noyau\fonctions\formater_date($projet['dateCreation']); ?>
  </p>

  <hr>

  <!-- Project One -->
  <div class="row">
    <div class="col-md-6">
      <a href="#">
        <img class="img-fluid rounded mb-3 mb-md-0" src="../www/images/<?php echo $projet['projetImage']; ?>" alt="">
      </a>
    </div>
    <div class="col-md-6">
      <p class="lead"><?php echo $projet['texte']; ?></p>
      <hr/>
      <p><?php echo $projet['texte']; ?></p>
      <hr/>
      <ul class="list-inline tags">
        <li><a href="#" class="btn btn-default btn-xs">Vintage</a></li>
        <li><a href="#" class="btn btn-default btn-xs">Football</a></li>
      </ul>
    </div>
  </div>
  <!-- /.row -->
  <hr>
