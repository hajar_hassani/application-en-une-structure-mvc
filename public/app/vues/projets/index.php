<?php
/*
  ./app/vues/projets/index.php
  Variables disponibles:
  - $projets: ARRAY(ARRAY(projetId, titre, texte, dateCreation, projetImage, creatif))
*/
?>

<!-- Page Heading -->

<!-- Title -->
<h1 class="mt-4">
  Les projets
  <small>Design capill'Hair</small>
</h1>

<hr>
<?php foreach ($projets as $projet): ?>
  <!-- Projets -->
<div class="row">
  <div class="col-md-4">
    <a href="#">
      <img class="img-fluid rounded mb-3 mb-md-0" src="../www/images/<?php echo $projet['projetImage']; ?>" alt="">
    </a>
  </div>
  <div class="col-md-8">
    <h3><?php echo $projet['titre']; ?></h3>
    <p class="lead">
      par
      <a href="artiste_details.html"><?php echo $projet['pseudo']; ?></a>
      le <?php echo \Noyau\fonctions\formater_date($projet['dateCreation']); ?>
    </p>
    <p><?php echo \Noyau\Fonctions\tronquer($projet['texte']); ?></p>
    <a class="btn btn-primary" href="projets/<?php echo $projet['projetId']; ?>/<?php echo \Noyau\fonctions\slugify($projet['titre']); ?>.html">View Project</a>
    <hr/>
    <ul class="list-inline tags">
      <?php
        if(isset($_GET['projetId'])):
          include_once '../app/controleurs/tagsControleur.php';
          \App\Controleurs\TagsControleur\tagByProjetAction($connexion, $_GET['projetId']);
        endif;
        ?>
    </ul>
  </div>
</div>
<!-- /.row -->
<hr>
<?php endforeach; ?>
