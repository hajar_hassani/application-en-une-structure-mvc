<?php
/*
  ./app/vues/creatifs/index.php
  Variables disponibles:
  - $creatifs: ARRAY(ARRAY(id, pseudo, bio, image))
*/
?>

<!-- Page Heading -->

<!-- Title -->
<h1 class="mt-4">
  Les Créa'tifs
  <small>Design capill'Hair</small>
</h1>

<hr>

<?php foreach ($creatifs as $creatif): ?>
  <!-- Projets -->
  <div class="row">
    <div class="col-md-4">
      <a href="#">
        <img class="img-fluid rounded mb-3 mb-md-0" src="../www/images/<?php echo $creatif['image']; ?>" alt="">
      </a>
    </div>
    <div class="col-md-8">
      <h3><?php echo $creatif['pseudo']; ?></h3>
      <p><?php echo $creatif['bio']; ?></p>
      <a class="btn btn-primary" href="projet_details.html">Voir détails</a>
      <hr/>
    </div>
  </div>
<hr/>
<?php endforeach; ?>
