<?php
/*
  ./app/vues/templates/defaut.php
 */
?>
<!DOCTYPE html>
<html lang="en">

  <head>
    <?php include '../app/vues/templates/partials/_head.php'; ?>
  </head>

  <body>

    <!-- Navigation -->
    <?php include '../app/vues/templates/partials/_nav.php'; ?>

    <!-- Page Content -->
    <?php include '../app/vues/templates/partials/_content.php'; ?>


    <!-- Footer -->
    <?php include '../app/vues/templates/partials/_footer.php'; ?>

    <!-- Bootstrap core JavaScript -->
    <?php include '../app/vues/templates/partials/_script.php'; ?>

  </body>

</html>
