<?php
/*
  ./app/vues/templates/partials/_nav.php
*/
?>

<!-- NAVIGATION -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
  <div class="container">
    <a class="navbar-brand" href="#">CREA'TIFs - Design capill'Hair</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active">
          <a class="nav-link" href="projets">Les projets
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="creatifs">Les CREA'TIFs</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
