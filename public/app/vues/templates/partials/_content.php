<?php
/*
  ./app/vues/templates/partials/_content.php
*/
?>

<!-- Page Content -->
<div class="container">

  <div class="row">

    <!-- Post Content Column -->
    <div class="col-lg-8">
      <?php echo $content1; ?>
    </div>

    <!-- Sidebar Widgets Column -->
    <div class="col-md-4">
      <?php include '../app/vues/templates/partials/_sidebar.php'; ?>
    </div>

  </div>
  <!-- /.row -->

</div>
<!-- /.container -->
