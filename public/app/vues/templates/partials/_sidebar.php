<?php
/*
./app/vues/templates/partials/_sidebar.php
*/
?>

<!-- Search Widget -->
<div class="card my-4">
  <h5 class="card-header">Newsletter</h5>
  <div class="card-body">
    <div class="input-group">
      <input type="text" class="form-control" placeholder="Votre mail">
      <span class="input-group-btn">
        <button class="btn btn-secondary" type="button">Go!</button>
      </span>
    </div>
  </div>
</div>

<!-- Categories Widget -->
<div class="card my-4">
  <h5 class="card-header">Tags</h5>
  <div class="card-body">
    <div class="row">
      <div class="col-lg-12">
        <?php include_once '../app/controleurs/tagsControleur.php';
        \App\Controleurs\TagsControleur\indexAction($connexion); ?>
      </div>

    </div>
  </div>
</div>
