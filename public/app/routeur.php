<?php
/*
  ./app/routeur.php
 */

 /*
   CHARGEMENT DU ROUTEUR DES PROJETS
   PATTERN: projets
 */
 if(isset($_GET['projets'])):
   include_once '../app/routeurs/projets.php';

/*
  CHARGEMENT DU ROUTEUR DES CREATIFS
  PATTERN: creatifs
*/
elseif(isset($_GET['creatifs'])):
  include_once '../app/routeurs/creatifs.php';

/*
  CHARGEMENT DU ROUTEUR DES TAGS
  PATTERN: tags/id/slug/edit/form.html
*/
elseif(isset($_GET['tags'])):
  include_once '../app/routeurs/tags.php';


 /*
   ROUTE PAR DEFAUT
   PATTERN: /
   CTRL: projetsControleur
   ACTION: index
 */

else:
 include_once '../app/controleurs/projetsControleur.php';
 \App\Controleurs\ProjetsControleur\indexAction($connexion);
endif;
