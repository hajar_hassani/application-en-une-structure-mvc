<?php
/*
    ./app/controleurs/creatifsControleur.php
*/
namespace App\Controleurs\CreatifsControleur;
use \App\Modeles\CreatifsModele AS Creatif;

 function indexAction(\PDO $connexion) {
   // Je demande la liste des créatifs au modèle
   include_once '../app/modeles/creatifsModele.php';
   $creatifs = Creatif\findAll($connexion);

   // Je charge la vue index dans $content1
   GLOBAL $content1, $title;
   $title = CREATIFS_INDEX_TITLE;
   ob_start();
     include '../app/vues/creatifs/index.php';
   $content1 = ob_get_clean();
 }
