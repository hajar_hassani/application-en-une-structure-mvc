<?php
/*
    ./app/controleurs/tagsControleur.php
*/
namespace App\Controleurs\TagsControleur;
use \App\Modeles\TagsModele AS Tag;

/**
 * [indexAction description]
 * @param  PDO    $connexion [description]
 * @return [type]            [description]
 */
 function indexAction(\PDO $connexion) {
   // Je demande la liste des tags au modèle
   include_once '../app/modeles/TagsModele.php';
   $tags = Tag\findAll($connexion);

   // Je charge la vue index directement
     include '../app/vues/tags/index.php';
 }


//LISTE DES TAGS PAR PROJET
function tagByProjetAction(\PDO $connexion, int $projetId) {
  //Je demande la liste des tags du projet au modeles
  include_once '../app/modeles/tagsModele.php';
  $tags = Tag\findTagsByProjetId($connexion, $projetId);

  //Je charge la vue indexByProjet directement
  include '../app/vues/tags/indexByProjet.php';
}


//MODIFICATION D'UN TAG
function editFormAction(\PDO $connexion, int $id) {
  //Je demande au modèle le tag à afficher dans le formulaire
  include_once '../app/modeles/tagsModele.php';
  $tag = Tag\findOneById($connexion, $id);

  //Je charge la vue editForm directement
  include '../app/vues/tags/index.php';
}
