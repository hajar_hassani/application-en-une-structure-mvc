<?php
/*
    ./app/controleurs/porojetsControleur.php
*/
namespace App\Controleurs\ProjetsControleur;
use \App\Modeles\ProjetsModele AS Projet;

 function indexAction(\PDO $connexion) {
   // Je demande la liste des projets au modèle
   include_once '../app/modeles/projetsModele.php';
   $projets = Projet\findAll($connexion);

   // Je charge la vue index dans $content1
   GLOBAL $content1, $title;
   $title = PROJETS_INDEX_TITLE;
   ob_start();
     include '../app/vues/projets/index.php';
   $content1 = ob_get_clean();
 }

function showAction(\PDO $connexion, int $id) {
   // Je demande le detail d'un projet au modèle
  include_once '../app/modeles/projetsModele.php';
  $projet = Projet\findOneById($connexion, $id);

  //Je charge la vue show dans $content1
  GLOBAL $content1, $title;
  $title = $projet['titre'];
  ob_start();
    include '../app/vues/projets/show.php';
  $content1 = ob_get_clean();
}
