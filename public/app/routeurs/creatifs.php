<?php
/*
    ./app/routeurs/creatifs.php
    Routes des creatifs
    Il existe un $_GET['creatifs']
*/

use App\Controleurs\CreatifsControleur;
include_once '../app/controleurs/creatifsControleur.php';

switch ($_GET['creatifs']) {
  /*
     LISTE DES CREATIFS
     PATTERN: creatifs
     CTRL:  creatifsControleur
     ACTION: index
 */
  case 'index':
    CreatifsControleur\indexAction($connexion);
    break;

  default:
    //code...
    break;
}
