<?php
/*
    ./app/routeurs/projets.php
    Routes des projets
    Il existe un $_GET['projets']
*/

include_once '../app/controleurs/projetsControleur.php';
use App\Controleurs\ProjetsControleur;

switch ($_GET['projets']):

  /*
     LISTE DES PROJETS
     PATTERN: projets
     CTRL:  projetsControleur
     ACTION: index
 */
  case 'index':
    ProjetsControleur\indexAction($connexion);
    break;
  /*
     DETAILS D'UN PROJET
     PATTERN: projets/id/slug.html
     CTRL:  projetsControleur
     ACTION: show
 */
  case 'show':
    ProjetsControleur\showAction($connexion, $_GET['id']);
    break;
endswitch;
