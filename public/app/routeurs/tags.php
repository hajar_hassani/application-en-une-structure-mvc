<?php
/*
    ./app/routeurs/tags.php
    Routes des tags
    Il existe un $_GET['tags']
*/

use App\Controleurs\TagsControleur;
include_once '../app/controleurs/tagsControleur.php';

switch ($_GET['tags']) {
  /*
     LISTE DES TAGS
     PATTERN: tags/id/slug/edit/form.html
     CTRL:  tagsControleur
     ACTION: index
 */
  case 'editForm':
    TagsControleur\editFormAction($connexion, $_GET['id']);
    break;

  default:
    //code...
    break;
}
