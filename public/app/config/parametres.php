<?php

// PARAMETRES DE CONNEXION A LA DB
  define('DB_HOST', 'localhost:3306');
  define('DB_NAME', 'creatifs');
  define('DB_USER', 'root');
  define('DB_PWD' , 'root');

// INITIALISATION DES ZONES DYNAMIQUES
   $content1 = '';
   $title    = '';

// AUTRES CONSTANTES
define('PROJETS_INDEX_TITLE', "Les projets");
define('CREATIFS_INDEX_TITLE', "Les Créa'tifs ");

define('FOLDER_PUBLIC', "public");
define('FOLDER_BACKOFFICE', "backoffice");
