<?php
/*
    ./app/modeles/projetsModele.php
*/
namespace App\Modeles\ProjetsModele;

function findAll(\PDO $connexion, int $nombreAfficher = 10) :array{
  $sql = "SELECT *, projets.id as projetId,
                    projets.image as projetImage
          FROM projets
          JOIN creatifs ON projets.creatif = creatifs.id
          ORDER BY dateCreation DESC
          LIMIT :nombre;";

$rs = $connexion->prepare($sql);
$rs->bindValue(':nombre', $nombreAfficher, \PDO::PARAM_INT);
$rs->execute();
return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

//DETAILS D'UN Projet
function findOneById(\PDO $connexion, int $id) :array {
  $sql = "SELECT *, projets.id as projetId,
                    projets.image as projetImage
          FROM projets
          JOIN creatifs ON projets.creatif = creatifs.id
          WHERE projets.id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
  return $rs->fetch(\PDO::FETCH_ASSOC);
}
