<?php
/*
    ./app/modeles/tagsModele.php
*/
namespace App\Modeles\TagsModele;

function findAll(\PDO $connexion){
  $sql = "SELECT *
          FROM tags
          ORDER BY nom DESC;";

$rs = $connexion->query($sql);
return $rs->fetchAll(\PDO::FETCH_ASSOC);
}

//LISTE DES TAGS PAR PROJET
function findTagsByProjetId(\PDO $connexion, int $projetId) :array {
  $sql = "SELECT tag, tags.nom as tagNom
          FROM projets_has_tags
          JOIN tags ON tag = tags.id
          WHERE projet = :projet;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':projet', $projetId, \PDO::PARAM_INT);
  $rs->execute();
  return $rs->fetch(\PDO::FETCH_ASSOC);
}


//MODIFICATION D'UN TAG
function findOneById(\PDO $connexion,int $id) :array {
  $sql = "SELECT *
          FROM tags
          WHERE id = :id;";
  $rs = $connexion->prepare($sql);
  $rs->bindValue(':id', $id, \PDO::PARAM_INT);
  $rs->execute();
  return $rs->fetch(\PDO::FETCH_ASSOC);
}
