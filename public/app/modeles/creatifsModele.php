<?php
/*
    ./app/modeles/creatifsModele.php
*/
namespace App\Modeles\CreatifsModele;

function findAll(\PDO $connexion){
  $sql = "SELECT *
          FROM creatifs
          ORDER BY id ASC;";

$rs = $connexion->query($sql);
return $rs->fetchAll(\PDO::FETCH_ASSOC);
}
