<?php
/*
  ./noyau/fonctions.php
  Fonctions personnalisées
 */
namespace Noyau\Fonctions;

/*
---------------------------------------------
TRAITEMENT DES CHAÎNES DE CARACTERES
---------------------------------------------
 */

/**
 * tronquer une chaîne au premier espace
 * après un certain nombre de caractères
 * @param  string  $chaine
 * @param  integer $nbreCaracteres [valeur par défaut]
 * @return string
 */
function tronquer(string $chaine, int $nbreCaracteres = 100) :string {
  if(strlen($chaine) > $nbreCaracteres):
    $positionEspace = strpos($chaine, ' ', $nbreCaracteres);
    return substr($chaine, 0, $positionEspace);
  else:
    return $chaine;
  endif;
}


/*
---------------------------------------------
TRAITEMENT DES DATES
---------------------------------------------
 */

/**
 * formater une date avec un format par défaut
 * @param  string $date
 * @param  string $format [fortmat par défaut]
 * @return string
 */
 function formater_date(string $date, string $format = "d-m-Y ") :string {
   return date_format(date_create($date), $format);
 }

/*
---------------------------------------------
TRAITEMENT DES CHAÎNES DE CARACTERES EN SLUG
---------------------------------------------
*/

function slugify(string $str) {
  $search = array('é', 'è', 'ê', 'à', 'ç', 'â');
  $replace = array('e', 'e', 'e', 'a', 'c', 'a');

  $slug = str_replace($search, $replace, trim(strtolower($str)));
  $slug = trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug), '-');

  return $slug;

}
