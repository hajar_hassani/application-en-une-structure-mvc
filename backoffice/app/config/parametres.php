<?php

// PARAMETRES DE CONNEXION A LA DB
  define('DB_HOST', 'localhost:3306');
  define('DB_NAME', 'creatifs');
  define('DB_USER', 'root');
  define('DB_PWD' , 'root');

// INITIALISATION DES ZONES DYNAMIQUES
   $content1 = '';
   $title    = '';

// AUTRES CONSTANTES
  define('USERS_DASHBOARD_TITLE', "Dashboard");
  define('POSTS_INDEX_TITLE', "Liste des posts");
  define('POSTS_ADDFORM_TITLE', "Ajout d'un post");
  define('POSTS_EDITFORM_TITLE', "Edition d'un post");

  define('FOLDER_PUBLIC', 'public');
  define('FOLDER_BACKOFFICE', 'backoffice');
